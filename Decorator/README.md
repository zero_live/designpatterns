# Decorator pattern

## Book Definition

- The decorator pattern is a design pattern that allows behavior to be added to an individual object, dynamically, without affecting the behavior of other objects from the same class.
- The decorator pattern is often useful for adhering to the Single Responsibility Principle, as it allows functionality to be divided between classes with unique areas of concern.
- The decorator pattern is structurally nearly identical to the chain of responsibility pattern, the difference being that in a chain of responsibility, exactly one of the classes handles the request, while for the decorator, all classes handle the request.

## What I understood

- Decorator wraps a class for add behaviour, can be implemented by dependence, by injection and by extension.

> I don't know if we are exending the class it has to work by its own for meet the Liskov principe.

## How I did it

I found different ways to implement this pattern, by dependence, by injection and by extension, I implemented the three ways, everyone of them is implemented in its folder.

After that I implemented the Weapon example I found in SourceMaking, you can find this example in `bookExample`.

And finally I implemented my example in the way I understood the pattern, you can found it in `myExample` folder.


## Documentation

- SourceMaking: https://sourcemaking.com/design_patterns/decorator
- Youtube Channel Christopher Okhravi: https://www.youtube.com/watch?v=GCraGHx6gso
- Blog Nithin Bekal: http://nithinbekal.com/posts/ruby-decorators/

# After review it at Devscola

- I understood that the implemetation I did in `bookExample` and `myExample` its wrong because I am adding different behaviour instad of add extra behaviour with the decorator, the reason is that I am decorating more than one base at same time instad of pass a single base to the decorator, you can see it in the classes `Weapom` and `Page`.
