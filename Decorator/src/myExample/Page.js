
const ELEMENTS_NEEDED_ERROR = 'Almost a element is needed'
const OPEN_HTML = '<html><head></head><body>'
const CLOSE_HTML = '</body></html>'

class Page {
  constructor() {
    this.elements = []
  }

  addElement(element) {
    this.elements.push(element)
  }

  output() {
    if(this.noElements()) { throw ELEMENTS_NEEDED_ERROR }
    let output = ''

    output += OPEN_HTML
    output += this.allElements()
    output += CLOSE_HTML

    return output
  }

  //private
  allElements() {
    let elements = ''

    this.elements.forEach((element) => {
      elements += element.output()
    })

    return elements
  }

  noElements() {
    return (this.elements.length == 0)
  }
}

module.exports = Page
