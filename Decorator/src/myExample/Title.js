
const TEXT_NEEDED_ERROR = 'Text is needed'
const CLOSE_TITLE = '</h1>'
const OPEN_TITLE = '<h1>'

class Title {
  constructor(text) {
    this.text = text
  }

  output() {
    if(this.noText()) { throw TEXT_NEEDED_ERROR }

    return this.textWithTitle()
  }

  //private
  textWithTitle() {
    const text = this.text.output()

    return OPEN_TITLE + text + CLOSE_TITLE
  }

  noText() {
    return (this.text == undefined)
  }
}

module.exports = Title
