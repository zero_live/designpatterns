
class Text {
  constructor(text) {
    this.text = text
  }

  output() {
    return this.text
  }
}

module.exports = Text
