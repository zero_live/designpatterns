const NEEDS_BASE_ERROR = 'A base is needed'

class Weapon {
  constructor() {
    this.parts = []
  }

  addBase(base) {
    this.parts.push(base)
  }

  damage() {
    if(this.thereAreNotBases()) { throw NEEDS_BASE_ERROR }
    let total = 0

    this.parts.forEach((part) => {
      total += part.damage()
    })

    return total
  }

  //private
  thereAreNotBases() {
    return (this.parts.length == 0)
  }
}

module.exports = Weapon
