const DAMAGE = 1

class BaseWeapon {
  damage() {
    return DAMAGE
  }
}

module.exports = BaseWeapon
