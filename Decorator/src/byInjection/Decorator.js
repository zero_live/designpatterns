const NO_BASE_ERROR = 'Base is needed'
const EXTRA_VALUE = 2

class Decorator {
  constructor(base) {
    this.base = base
  }

  output() {
    if (this.thereIsNotBase()) { throw NO_BASE_ERROR }

    return this.base.output() + EXTRA_VALUE
  }

  //private
  thereIsNotBase() {
    return (this.base == undefined)
  }
}

module.exports = Decorator
