
const NUMERIC_VALUE = 1

class Base {
  constructor() {
    this.value = NUMERIC_VALUE
  }

  output() {
    return this.value
  }
}

module.exports = Base
