const Base = require('./Base')

const EXTRA_VALUE = 2

class BaseWithDecorator extends Base {
  output() {
    return this.value + EXTRA_VALUE
  }
}

module.exports = BaseWithDecorator
