
const NUMERIC_VALUE = 1

class Base {
  output() {
    return NUMERIC_VALUE
  }
}

module.exports = Base
