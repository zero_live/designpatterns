const Base = require('./Base')

const EXTRA_VALUE = 2

class BaseWithDecorator {
  constructor() {
    this.base = new Base()
  }

  output() {
    return this.base.output() + EXTRA_VALUE
  }
}

module.exports = BaseWithDecorator
