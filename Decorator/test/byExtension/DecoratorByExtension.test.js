const BaseWithDecorator = require('../../src/byExtension/BaseWithDecorator')
const Base = require('../../src/byExtension/Base')
const expect = require('chai').expect

describe('Decorator Pattern by Extension', () => {
  describe('Base', () => {
    it('has a numeric output', () => {
      const numericOutput = 1
      const base = new Base()

      const output = base.output()

      expect(output).to.eq(numericOutput)
    })
  })

  describe('BaseWithDecorator', () => {
    it('adds behaviour to a Base', () => {
      const addedNumericOutput = 3
      const baseWithDecorator = new BaseWithDecorator()

      const output = baseWithDecorator.output()

      expect(output).to.eq(addedNumericOutput)
    })
  })
})
