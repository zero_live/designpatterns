const Decorator = require('../../src/byInjection/Decorator')
const Base = require('../../src/byInjection/Base')
const expect = require('chai').expect

describe('Decorator Pattern by Injection', () => {
  describe('Base', () => {
    it('has a numeric output', () => {
      const numericOutput = 1
      const base = new Base()

      const output = base.output()

      expect(output).to.eq(numericOutput)
    })
  })

  describe('Decorator', () => {
    it('adds behaviour to a Base', () => {
      const addedNumericOutput = 3
      const base = new Base()
      const decorator = new Decorator(base)

      const output = decorator.output()

      expect(output).to.eq(addedNumericOutput)
    })

    it('does not work without a base', () => {
      const decorator = new Decorator()

      expect(() => {
        decorator.output()
      }).throw('Base is needed')
    })
  })
})
