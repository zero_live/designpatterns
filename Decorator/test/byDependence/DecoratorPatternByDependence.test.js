const BaseWithDecorator = require('../../src/byDependence/BaseWithDecorator')
const Base = require('../../src/byDependence/Base')
const expect = require('chai').expect

describe('Decorator Pattern by Dependence', () => {
  describe('Base', () => {
    it('has a numeric output', () => {
      const numericOutput = 1
      const base = new Base()

      const output = base.output()

      expect(output).to.eq(numericOutput)
    })
  })

  describe('BaseWithDecorator', () => {
    it('adds behaviour to a Base', () => {
      const addedNumericOutput = 3
      const baseWithDecorator = new BaseWithDecorator()

      const output = baseWithDecorator.output()

      expect(output).to.eq(addedNumericOutput)
    })
  })
})
