const WeaponAccesories = require('../../src/bookExample/WeaponAccesories')
const BaseWeapon = require('../../src/bookExample/BaseWeapon')
const Weapon = require('../../src/bookExample/Weapon')
const {expect} = require('chai')

describe('Weapon Decorator book example', () => {
  describe('BaseWeapon', () => {
    it('has a damage', () => {
      const weaponDamage = 1
      const baseWeapon = new BaseWeapon()

      const damage = baseWeapon.damage()

      expect(damage).to.eq(weaponDamage)
    })
  })

  describe('WeaponAccesories', () => {
    it('has a damage', () => {
      const accesorieDamage = 2
      const weaponAccesorie = new WeaponAccesories()

      const damage = weaponAccesorie.damage()

      expect(damage).to.eq(accesorieDamage)
    })
  })

  describe('Weapon', () => {
    it('has a damage', () => {
      const weaponDamage = 3
      const weapon = new Weapon()
      const baseWeapon = new BaseWeapon()
      weapon.addBase(baseWeapon)
      const weaponAccesorie = new WeaponAccesories()
      weapon.addBase(weaponAccesorie)

      const damage = weapon.damage()

      expect(damage).to.eq(weaponDamage)
    })

    it('needs a base', () => {
      const weapon = new Weapon()

      expect(() => {
        weapon.damage()
      }).throw('A base is needed')
    })
  })
})
