const Title = require('../../src/myExample/Title')
const Page = require('../../src/myExample/Page')
const Text = require('../../src/myExample/Text')
const {expect} = require('chai')

describe('HTML Decorator', () => {
  describe('Text', () => {
    it('has a output as text', () => {
      const title = 'A Title'
      const text = new Text(title)

      const output = text.output()

      expect(output).to.eq(title)
    })
  })

  describe('Title', () => {
    it('adds a title element to a text', () => {
      const textBase = 'Text Base'
      const text = new Text(textBase)
      const title = new Title(text)

      const output = title.output()

      expect(output).to.eq(`<h1>${textBase}</h1>`)
    })

    it('needs a text', () => {
      const title = new Title()

      expect(() => {
        title.output()
      }).throw('Text is needed')
    })
  })

  describe('Page', () => {
    it('create a HTML page with elements', () => {
      const textBase = 'Text Base'
      const text = new Text(textBase)
      const title = new Title(text)
      const page = new Page()
      page.addElement(title)
      page.addElement(title)

      const output = page.output()

      expect(output).to.eq('<html><head></head><body><h1>Text Base</h1><h1>Text Base</h1></body></html>')
    })

    it('needs almost a element', () => {
      const page = new Page()

      expect(() => {
        page.output()
      }).throw('Almost a element is needed')
    })
  })
})
