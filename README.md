# Design Patterns

This is my repository for Design Patterns Study Group at Devscola

## Sytem dependencies

- `Docker version 18.06.1-ce` or higher.
- `docker-compose version 1.18.0` or higher.

## Prepare the project

In first place you have to build the project with docker-compose using:

- `docker-compose up --build`

## Running tests

After prepare the project you can run all the tests with:

- `sh run-tests.sh`

For run only Decorator pattern tests, use:

- `docker-compose run --rm decorator npm run test`

For run only Template Method pattern tests, use:

- `docker-compose run --rm template npm run test`

# Patterns list

- [Decorator](https://gitlab.com/zero_live/designpatterns/tree/master/Decorator)
- [Template Method](https://gitlab.com/zero_live/designpatterns/tree/master/TemplateMethod)
