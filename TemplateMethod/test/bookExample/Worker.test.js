const PostMan = require('../../src/bookExample/PostMan')
const Worker = require('../../src/bookExample/Worker')
const Coder = require('../../src/bookExample/Coder')
const {expect} = require('chai')

describe('Worker', () => {
  it('has a rutine', () => {
    const worker = new Worker()

    const rutine = worker.rutine()

    expect(rutine).to.deep.eq(['WakeUp', 'Work', 'Relax', 'Sleep'])
  })
})

describe('Coder', () => {
  it('change the work type', () => {
    const coder = new Coder()

    const rutine = coder.rutine()

    expect(rutine).to.deep.eq(['WakeUp', 'Code', 'Relax', 'Sleep'])
  })
})

describe('PostMan', () => {
  it('has most steps at work', () => {
    const postman = new PostMan()

    const rutine = postman.rutine()

    expect(rutine).to.deep.eq(['WakeUp', 'Move', 'Deliver', 'Relax', 'Sleep'])
  })
})
