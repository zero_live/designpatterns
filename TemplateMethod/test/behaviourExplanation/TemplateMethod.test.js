const TemplateMethod = require('../../src/behaviourExplanation/TemplateMethod')
const {expect} = require('chai')

describe('Template Method Pattern (Behaviour)', () => {
  it('ensures the result when process some steps', () => {
    const processResult = 3
    const template = new TemplateMethod()

    const result = template.process()

    expect(result).to.eq(processResult)
  })

  it("'s children can change the behaivour of a step", () => {
    const childProcessResult  = 4
    const child = new TestChild()

    const result = child.process()

    expect(result).to.eq(childProcessResult)
  })
})

const STEP_ONE_RESULT = 2

class TestChild extends TemplateMethod {
  stepOne() {
    this.result += STEP_ONE_RESULT
  }
}
