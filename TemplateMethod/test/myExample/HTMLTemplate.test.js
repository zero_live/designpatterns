const HTMLTemplate = require('../../src/myExample/HTMLTemplate')
const Child = require('../../src/myExample/Child')
const {expect} = require('chai')

describe('HTML Template', () => {
  it('builds a HTML template', () => {
    const html = new HTMLTemplate()

    const template = html.build()

    expect(template).to.eq('<html><head></head><body></body></html>')
  })
})

describe('Child', () => {
  it('builds with extra HTML', () => {
    const child = new Child()

    const template = child.build()

    expect(template).to.eq('<html><head><title>Child</title></head><body><h1>Welcome to Child page</h1></body></html>')
  })
})
