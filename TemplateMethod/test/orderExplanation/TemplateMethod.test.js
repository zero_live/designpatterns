const TemplateMethod = require('../../src/orderExplanation/TemplateMethod')
const {expect} = require('chai')

describe('Template Method Pattern (Order explanation)', () => {
  it('forces to implement the a step', () => {
    const template = new TemplateMethod()

    expect(() => {
      template.process()
    }).throw('First step has to be implemented')
  })

  it('forces to implement the follow step', () => {
    const childWithOneStep = new ChildWithOneStep()

    expect(() => {
      childWithOneStep.process()
    }).throw('Second step has to be implemented')
  })
})

class ChildWithOneStep extends TemplateMethod {
  firstStep() {}
}
