
class TemplateMethod {
  process() {
    this.firstStep()
    this.secondStep()
  }

  firstStep() {
    throw('First step has to be implemented')
  }

  secondStep() {
    throw('Second step has to be implemented')
  }
}

module.exports = TemplateMethod
