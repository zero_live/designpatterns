const HTMLTemplate = require('./HTMLTemplate')

class Child extends HTMLTemplate {
  header() {
    return '<head><title>Child</title></head>'
  }

  elements() {
    return '<h1>Welcome to Child page</h1>'
  }
}

module.exports = Child
