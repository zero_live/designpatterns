
class HTMLTemplate {
  constructor() {
    this.template = ''
  }

  build() {
    this.template += this.openHTML()
    this.template += this.header()
    this.template += this.openBody()
    this.template += this.elements()
    this.template += this.closeBody()
    this.template += this.closeHTML()

    return this.template
  }

  openHTML() {
    return '<html>'
  }

  header() {
    return '<head></head>'
  }

  openBody() {
    return '<body>'
  }

  elements() {
    return ''
  }

  closeBody() {
    return '</body>'
  }

  closeHTML() {
    return '</html>'
  }
}

module.exports = HTMLTemplate
