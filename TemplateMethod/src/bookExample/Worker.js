
class Worker {
  constructor() {
    this.rutineList = []
  }

  rutine() {
    this.addRutine(this.wakeUp())
    this.addRutine(this.work())
    this.addRutine(this.relax())
    this.addRutine(this.sleep())

    return this.rutineList
  }

  addRutine(rutine) {
    this.rutineList.push(rutine)
  }

  wakeUp() {
    return 'WakeUp'
  }

  work() {
    return 'Work'
  }

  relax() {
    return 'Relax'
  }

  sleep() {
    return 'Sleep'
  }
}

module.exports = Worker
