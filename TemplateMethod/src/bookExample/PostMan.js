const Worker = require('./Worker')

class PostMan extends Worker {
  work() {
    this.addRutine(this.move())

    return 'Deliver'
  }

  move() {
    return 'Move'
  }
}

module.exports = PostMan
