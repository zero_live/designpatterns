const Worker = require('./Worker')

class Coder extends Worker {
  work() {
    return 'Code'
  }
}

module.exports = Coder
