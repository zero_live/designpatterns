
const FIRST_STEP_RESULT = 1
const SECOND_STEP_RESULT = 2

class TemplatedMethod {
  constructor() {
    this.result = 0
  }

  process() {
    this.stepOne()
    this.stepTwo()

    return this.result
  }

  stepOne() {
    this.result += FIRST_STEP_RESULT
  }

  stepTwo() {
    this.result += SECOND_STEP_RESULT
  }
}

module.exports = TemplatedMethod
