# Template Method Pattern

## Book definition

- In software engineering, the template method pattern is a behavioral design pattern that defines the program skeleton of an algorithm in an operation, deferring some steps to subclasses. It lets one redefine certain steps of an algorithm without changing the algorithm's structure.

## What I understood

- This patter is used when we have to ensure that some steps happens.

## How I did it

I found different manners to use this patter, one for ensure a result and another for ensure the order of a process.

- The implementation where ensure a result is implemented in `behaviourExplanation` folder.
- The implementation where ensure an order is implemented in `orderExplanation` folder.
- I implemented the book example that use the behaviour implementation in `bookExample` is a use case with workers from SourceMaking.
- I implmented that I understood in `myExample` folder, it is a case use about HTML.

## Documentation

- SourceMaking: https://sourcemaking.com/design_patterns/template_method
- Youtube Channel Christopher Okhravi: https://www.youtube.com/watch?v=7ocpwK9uesw

# After review it at Devscola

- I understood rigth the concep of the patter, but in this pattern when is used to ensure an order you don't tests the order, you tests the result of the steps.
- In the implementation of the pattern in the `bookExample` is a bit extrange for the way I'm adding extra rutines in `PostMan#work`.
